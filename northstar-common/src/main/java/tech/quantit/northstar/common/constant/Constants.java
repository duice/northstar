package tech.quantit.northstar.common.constant;

public interface Constants {
	
	String INDEX_SUFFIX = "0000";

	String GATEWAY_CONTRACT_MAP = "GatewayContractMap";
	
	String KEY_USER = "USER";
	
	String PLAYBACK_GATEWAY = "PlaybackGateway";
	
	String PLAYBACK_MODULE_SUFFIX = "_Playback";
	
	// 资金占用比例估算系数
	double ESTIMATED_FROZEN_FACTOR = 1.5;
	
	String CTP_MKT_GATEWAY_ID = "CTP行情";
	
	String CTP_SIM_MKT_GATEWAY_ID = "CTP_SIM行情";
	
	String SIM_MKT_GATEWAY_ID = "SIM行情";
}
