package tech.quantit.northstar.strategy.api.constant;

public interface Bool {

	String TRUE = "true";
	
	String FALSE = "false";
}
